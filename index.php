<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="stylesheet.css"/>
        <title>Aresar</title>
    </head>
    <?php

// deklarace funkce pro úpravu textu, který bude zaslán do URL 
// - neřeší znaky ?, &, #, takže by nefungovalo správně, 
// tyto znaky jsou pro URL určující a je třeba je tedy filtrovat - ideálně funkce urlencode
    function uprav($text)
    {
        $return = Str_Replace(
            Array("á", "č", "ď", "é", "ě", "í", "ľ", "ň", "ó", "ř", "š", "ť", "ú", "ů", "ý ", "ž", "Á", "Č", "Ď", "É", "Ě", "Í", "Ľ", "Ň", "Ó", "Ř", "Š", "Ť", "Ú", "Ů", "Ý", "Ž"), Array("a", "c", "d", "e", "e", "i", "l", "n", "o", "r", "s", "t", "u", "u", "y ", "z", "A", "C", "D", "E", "E", "I", "L", "N", "O", "R", "S", "T", "U", "U", "Y", "Z"), $text);
        $return = Str_Replace(Array(" ", "_"), "-", $return);
        $return = Str_Replace(Array("(", ")", ".", "!", ",", "\"", "'"), "", $return);
        $return = StrToLower($return);
        return $return;
    }

    // Chybí v odevzdání - předpokládám, že je zde jen mysql_connect()    
    require_once('DTBconnect.php');

    $url = explode("/", $_SERVER['REQUEST_URI']);

    if ($url[1] == "novy-kontakt") {
        //---------------   nový kontakt   ----------------------------

        $kod = "<h2>Nový kontakt</h2>";

        $kod .= "<form method='post' name='novyKontakt'>";
        $kod .= " Jméno: <input type='text' name='jmeno'><br>";
        $kod .= " Telefon: <input type='text' name='tel'><br>";
        $kod .= " Email: <input type='text' name='email'><br>";
        $kod .= " Poznámka:<br>";
        $kod .= "<textarea name='poznamka' ></textarea><br><br>";
        $kod .= "<input type='submit' name='ulozKontakt' value='Uložit kontakt' /><br>";
        $kod .= "</form>";

        echo $kod;


        if ($_POST['ulozKontakt']) {

            $jmeno = $_POST['jmeno'];
            $tel = $_POST['tel'];
            $email = $_POST['email'];
            $poznamka = $_POST['poznamka'];

            $query = "INSERT INTO adresar (Jmeno,Telefon,Email,Poznamka) VALUES ('$jmeno','$tel', '$email','$poznamka')";
            $result = mysql_query($query) or die('Query failed: ' . mysql_error());

            header('Refresh: 0; url=\..');
        }
    } elseif ($url[1] == "edit") {
        //----------------   EDIT  + mazání  ----------------------------

        $urlArray = explode("-", $url[2]);
        $id = $urlArray[((count($urlArray)) - 1)];
// Přímý vstup z formuláře do databáze - útočník může smazat celou databázi nebo si nechat vypsat jiné tabulky z databáze atd... 
        $query = "SELECT * FROM adresar where ID=$id";
        $result = mysql_query($query) or die('Query failed: ' . mysql_error());
        $line = mysql_fetch_array($result, MYSQL_ASSOC);

        $kod = "<h2>Změna kontaktu: $line[Jmeno]</h2>"; // chyba v přístupu do pole - správný zápis {$line['Jmeno']}

        $kod .= "<form method='post' name='zmenaKontaktu'>";
        $kod .= " Jméno: <input type='text' name='jmeno' value='$line[Jmeno]'><br>";
        $kod .= " Telefon: <input type='text' name='tel' value='$line[Telefon]'><br>";
        $kod .= " Email: <input type='text' name='email' value='$line[Email]'><br>";
        $kod .= " Poznámka:<br>";
        $kod .= "<textarea name='poznamka' >$line[Poznamka]</textarea><br><br>";
        $kod .= "<input type='submit' name='ulozZmeny' value='Uložit změny' /><br><br>";
        $kod .= "<input type='submit' name='smazKontakt' value='Smazat kontakt' /><br><br>";
        $kod .= "</form>";

        echo $kod;



        if ($_POST['ulozZmeny']) {

// Přímý vstup z formuláře do databáze - útočník může smazat celou databázi nebo jinak uškodit 
            $jmeno = $_POST['jmeno'];
            $tel = $_POST['tel'];
            $email = $_POST['email'];
            $poznamka = $_POST['poznamka'];

            $query = "UPDATE adresar SET 
			Jmeno = '$jmeno',
			Telefon = '$tel',
			Email = '$email',
			Poznamka = '$poznamka'
			WHERE (ID = '$id');";

            $result = mysql_query($query) or die('Query failed: ' . mysql_error());
            header('Refresh: 0; url=\..');
        }


        if ($_POST['smazKontakt']) {

            $query = "DELETE FROM adresar WHERE (ID = $id)";
            $result = mysql_query($query) or die('Query failed: ' . mysql_error());

            header('Refresh: 0; url=\..');
        }
    } else {
        //--------------- default   - tabulka kontaktů--------------------------------------------------------------------------------------------

        $query = 'SELECT * FROM adresar ORDER BY ID ASC';
        $result = mysql_query($query) or die('Query failed: ' . mysql_error());

        $kod = "<h2>Adresář</h2>";
        $kod .= "\n<table>\n";
        $kod .= "<thead><th> Jméno  a Príjmení </th><th> Telefon </th><th> Email </th><th> Poznámka </th></thead>\n";

        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {

            //sestavení upravené(bez diakritiky) URL /.../edit/jmeno-prijmeni-ID
            $identifikator = uprav($line[Jmeno]) . '-' . $line[ID];
// Přímý výpis údajů z databáze - vzhledem k tomu, že všichni uživatelé vidí to samé, 
// je zde možné XSS, kdy uživatel vloží v rámci editace škodlivý JavaScript kód a ten 
// je pak zobrazen každému.. 
            $kod .= "\t<tr>\n";
            $kod .= "\t\t<td style='border: 1px solid black;'>$line[Jmeno]</td>";
            $kod .= "\t\t<td style='border: 1px solid black;'>$line[Telefon]</td>";
            $kod .= "\t\t<td style='border: 1px solid black;'>$line[Email]</td>";
            $kod .= "\t\t<td style='border: 1px solid black;'>$line[Poznamka]</td>";
            $kod .= "</td><td><a href='/edit/" . $identifikator . "/'>Edit</a></td>";
        }

        $kod .= "</table>\n";
        $kod .= "</br>";

        $kod .= "<a href='/novy-kontakt/'><input type='button' value=' Vytvořit nový kontakt '></a>";

        echo $kod;
    }
    ?>











